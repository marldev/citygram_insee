/* PLAYER MODULES */
"LOADING PLAYER MODULES".postln;


/* PBIND VOICES */
// These manage the pattern players
// They call the \noteCreator SynthDef and supply it with the needed arguments
// Frequency is set by the synth, and then converted to the
// correct bufnum holding the sample. This bufnum, along with the freq/midinote
// are passed to the synth.

~see.melodyPtr0 = 0;
~see.melodyPtr1 = 0;
~see.melodyPtr2 = 0;
~see.melodyArrays = [
	[ [ 0, 0, 0, 2 ], 					[1, 1, 1, 2] ],
	[ [ 7, 7, 7, 0 ], 					[1, 1, 1, 2] ],
	[ [ 4, 2, 4, 2 ], 					[1, 1, 2, 1] ],
	[ [ 5, 7, 8, 11 ], 					[2, 2, 2, 1] ],
	[ [ 0, -1, -5, -4, -2, -1 ],		[1, 1, 2, 3, 2, 1] ],
	[ [ 1, 0, -1, 1, 0, -1, -5, -5 ],	[0.5, 1, 0.5, 0.5, 1, 0.5, 3, 1 ] ],
	[ [ 7, 5, 7, 5, 7, 5 ], 			[0.5, 1, 0.5, 2, 1, 2 ] ],
	[ [ 0, 12, 2, 11, 4, 9, 5, 7 ], 	[ 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5 ] ],
	[ [ 0, 12, 2, 11, 4, 9, 5, 7 ], 	[ 1, 0.5, 0.25, 0.25, 0.5, 1, 2, 4 ] ],
	[ [ 11, 11, 10, 11 ], 				[ 2, 2, 2, 2 ] ],
	[ [ 7, 6, 6, 0, 6, 12, 6 ], 		[ 1, 2, 2, 2, 1, 3, 2 ] ],
	[ [ 7, 5, 4, 2, 7, 9 ],				[1, 1, 1, 1, 1, 1] ],
	[ [ 7, 5, 3, 3, 3, 3 ],				[1, 1, 1, 2, 2, 1] ],
	[ [ 7, 5, 4, 8, 8, 11, 11 ],		[2, 0.5, 0.5, 1, 1, 1] ],
	[ [ 7, 1, 11, 20, 6, 18, 3 ], 		[ 0.5, 0.25, 0.25, 1 ] ],
	[ [ 0, 7, 5, 5, 0, 7, 12, 7 ], 		[ 1, 2, 2, 2, 1, 3, 2 ] ],
	[ [ 0, 12, 0, 6 ], 					[ 1, 2, 2, 2 ] ],
	[ [ 0, -1, -2, -3, -1 ],			[ 1, 2, 2, 2 ] ],
	[ [ 0, 2 ],							[ 2, 2 ] ]
];


~see.autoChangeNoteSeq.stop;
~see.autoChangeNoteSeq = Task({
	var waitTime;

	inf.do({
		waitTime = 60.rand;

		if( 0.5.coin, {
			// The scale degrees to use
			Pdefn(\degree0,
				Pseq(~see.melodyArrays[~see.melodyPtr0][0], inf);
			);
			Pdefn(\durLower,
				Pseq(
					(~see.melodyArrays[~see.melodyPtr0][1]
						* ~see.playerTimeMulLower),
					inf)
			);
			~see.melodyPtr0 = (
				(~see.melodyPtr0+1) % ~see.melodyArrays.size
			);
		});

		if( 0.5.coin, {
			Pdefn(\degree1,
				Pseq(~see.melodyArrays[~see.melodyPtr1][0], inf);
			);
			// The durations of each notes
			Pdefn(\durHigher,
				Pseq(
					(~see.melodyArrays[~see.melodyPtr1][1]
						* ~see.playerTimeMulHigher),
					inf)
			);
			~see.melodyPtr1 = (
				(~see.melodyPtr1+1) % ~see.melodyArrays.size
			);

		});

		if( 0.5.coin, {

			// The scale degrees to use
			Pdefn(\degree2,
				Pseq(~see.melodyArrays[~see.melodyPtr2][0], inf);
			);

			Pdefn(\durHigher2,
				Pseq(
					(~see.melodyArrays[~see.melodyPtr2][1]
						* ~see.playerTimeMulLower),
					inf)
			);

			~see.melodyPtr2 = (
				(~see.melodyPtr2+1) % ~see.melodyArrays.size
			);

		});

		waitTime.wait;
	});

});

~see.autoRootChange.stop;
~see.autoRootChange = Task({
	var waitTime, root;
	root = 0;
	inf.do({
		waitTime = 10.rand;

		// 1/5 of the time, the root should go up a semitone
		if( (1/4).coin,
			{
				root = (root + 7) % 12;
				Pdefn(\root, root);
				// "root changed".postln;
			},
			{
				// "root no changy".postln;
			}
		);

		waitTime.wait;
	});
});

/* HARMONY VOICE I. */
~restRatio = 0.8;
Pdef(
	key: \voice1,
	item: Pbind(
		\instrument, \noteCreator,
		// chromatic scale
		\scale, Pdefn(\scale, #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]),
		// always based on C
		\root, Pdefn(\root, 0),
		// initial Octave choise
		\octave, Pdefn(\octave1, 4),
		// The scale degrees to use
		\degree, Pdefn(\degree0,
			Pseq(~see.melodyArrays[~see.melodyPtr1][0], inf);
		),
		// The durations of each notes
		\dur, Pdefn(\durLower,
			Pxrand(~see.melodyArrays[~see.melodyPtr1][1], inf)
		),
		\dur1, Pfunc { |ev| ev[\dur] },
		// the ratio of noteSustain -> dur
		\attackTime, Pdefn(\attack1, 0.8),
		\legato, Pdefn(\legato1, 0.2),
		// \sustain, Pdefn(\sustain1, 1),
		// \length, Pdefn(\length1, 1),
		// \sigMixComb, Pdefn(\sigMixComb1, 0.95),
		\sigMixSin, Pdefn(\sigMixSin, 0.6),
		// note amplitude
		\mainAmp, Pdefn(\mainAmp),
		\amp, Pdefn(\amp1, 0.4),

		// Position in the stereo field
		\pos, Pdefn(\pos1, Pfunc{ rrand(-1.0, 1.0) }),
		\out, Pdefn(\out1, ~see.playerVerbBus),
		// get the freq
		\freq, Pfunc { |ev| ev.use(ev[\freq]) },
		// use the frequency to determine th correct bufnum
		// since these can be destroyed/created often, there is no
		// way of guaranteeing bufnums will stay constant or sequential
		// Therefore this function checks if a sample exists
		// at that 'note' in the array
		// and if it does, it returns the bufnum, which can then
		// be passed to the SynthDef
		// if no sample exists, then the note is treated as a 'rest'
		\bufnum, Pfunc({
			|ev|
			var test, value, restTest, note;
			note = ev[\freq].cpsmidi.round.asInteger;
			// "1 - ".post; note.post; " : ".post;
			value = ~noteRec.noteBuffs.at(note);
			if( value.isNil,
				{
					// ev[\degree].postln;
					// "not available : ".post;
					ev[\degree] = 'rest';
					ev[\freq] = 'rest';
					value = 0;
				},{
					value = value.bufnum;
				}
			);

			// should this be a rest?
			restTest = Pwrand(
				[true, false], [~restRatio, (1-~restRatio)],
				1
			).asStream.next;
			if( restTest, {
				ev[\degree] = 'rest';
				ev[\freq] = 'rest';
			});
			// "Lower - ".post;
			// ev[\degree].post; "/".post; note.post;
			// " : ".post; ev[\octave].post;
			// " : ".post; ev[\root].post;
			// (" :: " ++ ~see.melodyPtr0).postln;
			// ev.postln; "".postln; "".postln;
			value
		})
	)
);


Pdef(
	key: \voiceHigh,
	item: Pbind(
		\instrument, \noteCreator,
		// chromatic scale
		\scale, Pdefn(\scale, #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]),
		// always based on C
		\root, Pdefn(\root, 0),
		// initial Octave choise
		\octave, Pdefn(\octaveHigh, 4),
		// The scale degrees to use
		\degree, Pdefn(\degree1,
			Pseq(~see.melodyArrays[~see.melodyPtr1][0], inf);
		),
		// The durations of each notes
		\dur, Pdefn(\durHigher,
			Pxrand(~see.melodyArrays[~see.melodyPtr1][1], inf)
		),
		\dur1, Pfunc { |ev| ev[\dur] },
		// the ratio of noteSustain -> dur
		\attackTime, Pdefn(\attackHigh, 0.1),
		\legato, Pdefn(\legatoHigh1, 0.2),
		// \sustain, Pdefn(\sustainHigh, 1),
		// \length, Pdefn(\lengthHigh, 1),
		// \sigMixComb, Pdefn(\sigMixComb1, 0.95),
		\sigMixSin, Pdefn(\sigMixSin, 0.6),
		// note amplitude
		\mainAmp, Pdefn(\mainAmpHigher),
		\amp, Pdefn(\amp1, 0.4),

		// Position in the stereo field
		\pos, Pdefn(\posHigh, Pfunc{ rrand(-1.0, 1.0) }),
		\out, Pdefn(\out1, ~see.playerVerbBus),
		// get the freq
		\freq, Pfunc { |ev| ev.use(ev[\freq]) },
		// use the frequency to determine th correct bufnum
		// since these can be destroyed/created often, there is no
		// way of guaranteeing bufnums will stay constant or sequential
		// Therefore this function checks if a sample exists
		// at that 'note' in the array
		// and if it does, it returns the bufnum, which can then
		// be passed to the SynthDef
		// if no sample exists, then the note is treated as a 'rest'
		\bufnum, Pfunc({
			|ev|
			var test, value, restTest, note;
			note = ev[\freq].cpsmidi.round.asInteger;
			// "1 - ".post; note.post; " : ".post;
			value = ~noteRec.noteBuffs.at(note);
			if( value.isNil,
				{
					// ev[\degree].postln;
					// "not available : ".post;
					ev[\degree] = 'rest';
					ev[\freq] = 'rest';
					value = 0;
				},{
					value = value.bufnum;
				}
			);

			// should this be a rest?
			restTest = Pwrand([true, false], [~restRatio, (1-~restRatio)], 1).asStream.next;
			if( restTest, {
				ev[\degree] = 'rest';
				ev[\freq] = 'rest';
			});
			// "Higher1 - ".post;
			// ev[\degree].post; "/".post; note.post;
			// " : ".post; ev[\octave].post;
			// " : ".post; ev[\root].post;
			// (" :: " ++ ~see.melodyPtr1).postln;
			// ev.postln; "".postln; "".postln;
			value
		})
	)
);



Pdef(
	key: \voiceHigh2,
	item: Pbind(
		\instrument, \noteCreator,
		// chromatic scale
		\scale, Pdefn(\scale, #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]),
		// always based on C
		\root, Pdefn(\root, 0),
		// initial Octave choise
		\octave, Pdefn(\octaveHigh2, 4),
		// The scale degrees to use
		\degree, Pdefn(\degree2,
			Pseq(~see.melodyArrays[~see.melodyPtr2][0], inf);
		),
		// The durations of each notes
		\dur, Pdefn(\durHigher2,
			Pxrand(~see.melodyArrays[~see.melodyPtr2][1], inf)
		),
		\dur1, Pfunc { |ev| ev[\dur] },
		// the ratio of noteSustain -> dur
		\attackTime, Pdefn(\attackHigh, 0.1),
		\legato, Pdefn(\legatoHigh1, 0.2),
		// \sustain, Pdefn(\sustainHigh, 1),
		// \length, Pdefn(\lengthHigh, 1),
		// \sigMixComb, Pdefn(\sigMixComb1, 0.95),
		\sigMixSin, Pdefn(\sigMixSin, 0.6),
		// note amplitude
		\mainAmp, Pdefn(\mainAmpHigher),
		\amp, Pdefn(\amp1, 0.4),

		// Position in the stereo field
		\pos, Pdefn(\posHigh, Pfunc{ rrand(-1.0, 1.0) }),
		\out, Pdefn(\out1, ~see.playerVerbBus),
		// get the freq
		\freq, Pfunc { |ev| ev.use(ev[\freq]) },
		// use the frequency to determine th correct bufnum
		// since these can be destroyed/created often, there is no
		// way of guaranteeing bufnums will stay constant or sequential
		// Therefore this function checks if a sample exists
		// at that 'note' in the array
		// and if it does, it returns the bufnum, which can then
		// be passed to the SynthDef
		// if no sample exists, then the note is treated as a 'rest'
		\bufnum, Pfunc({
			|ev|
			var test, value, restTest, note;
			note = ev[\freq].cpsmidi.round.asInteger;
			// "1 - ".post; note.post; " : ".post;
			value = ~noteRec.noteBuffs.at(note);
			if( value.isNil,
				{
					// ev[\degree].postln;
					// "not available : ".post;
					ev[\degree] = 'rest';
					ev[\freq] = 'rest';
					value = 0;
				},{
					value = value.bufnum;
				}
			);

			// should this be a rest?
			restTest = Pwrand([true, false], [~restRatio, (1-~restRatio)], 1).asStream.next;
			if( restTest, {
				ev[\degree] = 'rest';
				ev[\freq] = 'rest';
			});
			// "Higher2 - ".post;
			// ev[\degree].post; "/".post; note.post;
			// " : ".post; ev[\octave].post;
			// " : ".post; ev[\root].post;
			// (" :: " ++ ~see.melodyPtr2).postln;
			// ev.postln; "".postln; "".postln;
			value
		})
	)
);


/* VERB TAIL FOR PLAYER SYNTH */


SynthDef(\playerTail, {
	arg out = 0, sigIn = 10, stretchSigIn = 12, revTime = 8,
		damp = 0.6, spread = 40, drylevel = 1,
		earlyreflev = 0.1, taillevel = 0.1, roomsize = 40;
	var sig, stretchSig, stretchAmp;

	sig = In.ar(sigIn, 2);


	stretchSig = In.ar(stretchSigIn, 2);
	stretchAmp = Amplitude.kr(
		in: stretchSig[0],
		attackTime: 0.01,
		releaseTime: 1
	);
	stretchAmp = (stretchAmp * 10).clip(0,1).lag2ud( 0.05, 4 );

	// alter rhythmic sig based on stretch sig amplitude
	sig = sig * (1-stretchAmp);

	// Combine them
	// stretchSig = stretchSig ! 2;
	sig = Mix([ sig, stretchSig ]);

	// RunningSum.rms(sig, 2048).poll;

	sig = GVerb.ar(
		sig,
		roomsize: roomsize.lag(2),
		revtime: revTime.lag(2),
		damping: damp,
		spread: spread,
		drylevel: drylevel * (1-stretchAmp),
		earlyreflevel: earlyreflev,
		taillevel: (taillevel * (stretchAmp).linlin(0, 1, 1, 20 )).clip(0, 1),
		maxroomsize: 150
	);

	sig = Limiter.ar( sig, 1, 0.005 );

	Out.ar( out, sig );

	// record out
	Out.ar( 4, sig );
}
).add;



/* noteCreator SynthDef */
// This Synth is given a bufnum and plays the relevant stored note
(
SynthDef(\noteCreator,
	{
		arg bufnum = 60, freq = 400, midinote = 60,
			amp = 0.7, length = 1, pos = 0, out = 2,
			gate = 1, rq = 0.9, sigMixSin = 0.6,
			mainAmp = 0.6, attackTime = 1, dur1 = 1;
		var sig, sigSin, env, fFreq, hFreq, ratio;
		var resSig;

		// testing OSC sendTrig
		SendReply.kr(
			trig: Line.kr(-1, 1, 0.0001),
			cmdName: '/oscWorkAround',
			values: [bufnum, midinote, freq]
		);

		// The signal comes from a buffer
		sig = PlayBuf.ar(
			numChannels: 1,
			bufnum: bufnum,
			rate: 1,
			trigger: 1,
			startPos: 0,
			loop: 0
		);

		resSig = Resonz.ar(
			in: sig,
			freq: freq,
			bwr: 0.3
		);

		resSig = Normalizer.ar(
			in: resSig,
			level: 0.9,
			dur: 0.001
		);

		// #fFreq, hFreq = Pitch.kr(
		// 	in: sig,
		// 	initFreq: freq,
		// 	minFreq:60,
		// 	maxFreq: 2000
		// );
		//
		// ratio = fFreq / freq;
		//
		// sig = PitchShift.ar(
		// 	in: sig,
		// 	windowSize: 0.1,
		// 	pitchRatio: ratio,
		// 	pitchDispersion: 0,
		// 	timeDispersion: 0.01
		// ) * 1.1;

		sig = RHPF.ar(
			in: sig,
			freq: freq,
			rq: 0.99
		);
		sig = HighShelf.ar(
			in: sig,
			freq: freq*2,
			shelfslope: 2,
			dbgain: -3
		);


		sig = Mix([ sig, resSig * 0.4 ]);


		// Compressor
		sig = sig * 2;
		sig = CompanderD.ar(
			sig,
			0.5,
			1,
			(1/3),	// compression level;
			clampTime: 0.01,
			relaxTime: 0.01
		);

		sig = Limiter.ar( sig, 0.97, 0.01 );


		sigSin = SinOsc.ar( [freq, freq*2], mul: 0.3 );
		sigSin = DelayN.ar( sigSin, 0.1, 0.03 );
		sigSin = Mix( [sigSin[0]*0.8, sigSin[1]*0.4])*0.7;

		sig = Mix([
			(sig * (sigMixSin.clip(0, 1))),
			(sigSin * (1-sigMixSin.clip(0, 1)))
		]);


		// define an envelope
		env = Env.new(
			levels: [0, 1, 0.6, 0.4, 0],
			times: [attackTime, 0.3, 0.25, attackTime].normalizeSum * dur1,
			curve: 0,
			releaseNode: 3
		);

		// place the envelope into a control signal
		env = EnvGen.kr(
			env,
			gate: gate,
			// levelScale: amp * grpLev * mainAmp,
			levelScale: amp * mainAmp,
			timeScale: length,
			doneAction: 2
		);
		// multiply the signal by the envelope
		sig = sig * env;

		// place the signal into a stereo field
		sig = Pan2.ar( sig, pos: pos );

		DetectSilence.ar( sig, doneAction: 2);

		// send that sucker out the speakers
		Out.ar( out, sig );
		// Out.ar( out+4, sig );

		Line.kr(0, 1, 10, doneAction: 2);
	}
).add;
);

" -- PLAYER MODULES LOADED".postln;