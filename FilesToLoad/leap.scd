/* LEAP OSC */

"LOADING LEAP MOTION DEFS".postln;

(


	// leapData
	~leap.x = 0;
	~leap.y = 0;
	~leap.z = 0;
	~leap.handspread = 0;
	~leap.timestamp = 0;
	~leap.canTrig = false;

OSCdef( \leapPos, {
		|msg, time, addr, recvPort|
		var yVal, tempo;
		yVal = msg[2];
		tempo = msg[3];
		// msg.postln;
		// addr.postln;
		// recvPort.postln;

		// yVal.postln;
		if( (yVal > 0.8),
			{
				if( ~leap.canTrig, {
					"trig new pattern".postln;
					~leap.canTrig = false;
					// The scale degrees to use
					Pdefn(\degree0,
						Pseq(~see.melodyArrays[~see.melodyPtr0][0], inf);
					);
					Pdefn(\durLower,
						Pseq(
							(~see.melodyArrays[~see.melodyPtr0][1]
								* ~see.playerTimeMulLower),
							inf)
					);
					~see.melodyPtr0 = (
						(~see.melodyPtr0+1) % ~see.melodyArrays.size
					);

					Pdefn(\degree1,
						Pseq(~see.melodyArrays[~see.melodyPtr1][0], inf);
					);
					// The durations of each notes
					Pdefn(\durHigher,
						Pseq(
							(~see.melodyArrays[~see.melodyPtr1][1]
								* ~see.playerTimeMulHigher),
							inf)
					);
					~see.melodyPtr1 = (
						(~see.melodyPtr1+1) % ~see.melodyArrays.size
					);


					// The scale degrees to use
					Pdefn(\degree2,
						Pseq(~see.melodyArrays[~see.melodyPtr2][0], inf);
					);

					Pdefn(\durHigher2,
						Pseq(
							(~see.melodyArrays[~see.melodyPtr2][1]
								* ~see.playerTimeMulLower),
							inf)
					);

					~see.melodyPtr2 = (
						(~see.melodyPtr2+1) % ~see.melodyArrays.size
					);
				});
			}, {
				~leap.canTrig = true;
			}

		);

		~leap.timestamp = SystemClock.seconds;


		// Set the time stamp
		tempo = tempo.linlin(0, 1, 180, 52 );
	TempoClock.default.tempo = (tempo.clip(52, 200) / 60);
	},
	'/h1/',
	nil,
	53000
).permanent_(true);

OSCdef( \leapAxis, {
		|msg, time, addr, recvPort|

		var roll;
		roll = msg[2];
		// msg.postln;
		// addr.postln;
		// recvPort.postln;

		// roll -> attack time
		// roll.postln;
		Pdefn(\attack1, roll.abs.linlin(0, 1.7, 5, 0.01) );

		~leap.timestamp = SystemClock.seconds;
	},
	'/h1axis/',
	nil,
	53000
).permanent_(true);

OSCdef( \leapHand, {
		|msg, time, addr, recvPort|

		// msg.postln;
		// addr.postln;
		// recvPort.postln;
		~leap.timestamp = SystemClock.seconds;
	},
	'/h1d/',
	nil,
	53000
).permanent_(true);






~leap.timerTask  = {
	Task({
		arg elapsedTime;
		var autoBarkTime = 120*10;
		10.wait;
		inf.do({
			elapsedTime = SystemClock.seconds - ~leap.timestamp;
			if(elapsedTime > ~leap.maxWait,
				{
					Pdefn(\mainAmp, 0.0);
					Pdefn(\attack1,
						(~see.cgPuller.slopeVal).abs
						.explin(0, 0.001, 0.001, 1).clip(0, 1)
					);

					// randomly reset elapsed time from time-to-time
					if( elapsedTime > autoBarkTime, {
						~leap.timestamp = SystemClock.seconds;
						autoBarkTime = 240.rand * 100;
					});

				},
				{
					Pdefn(\mainAmp, ~see.playerMainAmp );
					Pdefn(\mainAmpHigher, ~see.playerMainAmp * 0.7 );
				}
			);
			// elapsedTime.postln;
			0.1.wait;
		});
	});
};

// TODO: turn the thing off gracefully
// ~leap.




);