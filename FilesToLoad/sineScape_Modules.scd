// -----------------------------------------------------------------------
//                       InSeE Etude 1: Sinescape
// -----------------------------------------------------------------------
"LOADING SINESCAPE MODULES".postln;
// -----------------------------------------------------------------------
// Synthe Defs
// -----------------------------------------------------------------------
(
SynthDef.new("Sinescape",
	{
		arg outBus = 0, density = 50, bwFreqStart = 100.0,
		    amp = 30, effectBus, direct = 0.5, ampMaster = 0.3,
		    ampJitter = 0;
		var sig, scalerDensity = 0.050, scalerAmp = 10;

		// create resonance sound objects with dust
		sig = Mix.ar(
			Array.fill(20, {
				var resBwr, resMult = 0.003;

				// bandwidth setup
				resBwr = (bwFreqStart + (12.rand)*bwFreqStart).clip(40, 20000);

				Resonz.ar(
					Dust.ar(density*scalerDensity, (amp*scalerAmp+ampJitter) ),
					[resBwr, resBwr*2, resBwr*3],
					[resMult, resMult/2, resMult/3]
				);
			});
		);

		// sound ouput
		Out.ar(outBus, sig*direct*ampMaster);
		Out.ar(effectBus, sig*(1-direct)*ampMaster);
}).add;

SynthDef.new("sinGliss",
	{
		arg outBus = 0, effectBus, impulseFreq = 0.1,
		    ampMaster = 0.6, freq = 220, freqDecay = 200;
		var imp, sig, direct = 0.2, attackTime = 0.1, decayTime = 1;

		imp = EnvGen.kr(Env.perc, 2.0, doneAction: 2)*SinOsc.ar([freq, freq*2, freq*3],0,0.1);

		sig = Decay2.ar(imp, attackTime, decayTime,
			SinOsc.ar(SinOsc.kr(0.01, 0, 110, freqDecay)));

		sig = Limiter.ar( sig, 0.4, 0.01 );

		Out.ar(outBus, sig*direct*ampMaster);
		Out.ar(effectBus, sig*(1-direct)*ampMaster);
}).add;

SynthDef.new("myReverb",
	{
		arg outBus = 0, inBus, direct = 0.5, ampMaster = 0.1;
		var outDelay, sig, input;

		input = In.ar(inBus, 1);

		// reverb predelay time :
		outDelay = DelayN.ar(input, 0.048);

		// 7 length modulated comb delays in parallel :
		sig = Mix.ar(Array.fill(7,
			{CombL.ar(outDelay, 0.1, LFNoise1.kr(0.1.rand, 0.04, 0.05), 15/4) }));

		// two parallel chains of 4 allpass delays (8 total) :
		4.do({ sig = AllpassN.ar(sig, 0.050, [0.050.rand, 0.050.rand], 1) });

		Out.ar(outBus, sig*ampMaster);
	}
).add;

)



